#include <stdio.h>

void f(double * d)
{
    *d = 4;
}

int main(int argc, char* argv[])
{
    double d;
    double * pDouble;
    d = 3;
    pDouble = &d;

    f(pDouble);
    printf("%f\n", *pDouble);
    return 0;
}