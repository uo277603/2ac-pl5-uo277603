#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Javier;
    PPerson pJavier;

pJavier = &Javier;
    Javier.heightcm = 180;
    Javier.weightkg = 84.0;
    pJavier->weightkg = 83.2;

    printf("Peter's height: %d",Peter.heightcm);
    printf(" cm: Peter's weight: %f", Peter.weightkg);
    printf(" kg");

    return 0;
}